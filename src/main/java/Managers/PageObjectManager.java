package Managers;

import PageObjects.ArticlePage;
import PageObjects.HomePage;
import PageObjects.LoginPage;
import org.openqa.selenium.WebDriver;

public class PageObjectManager {

    private final WebDriver driver;
    private ArticlePage articlePage;
    private HomePage homePage;
    private LoginPage loginPage;

    public PageObjectManager(WebDriver webDriver) {
        this.driver = webDriver;
    }

    public ArticlePage getArticlePage() {
        return (articlePage == null) ? articlePage = new ArticlePage(driver) : articlePage;
    }
    public HomePage getHomePage() {
        return (homePage == null) ? homePage = new HomePage(driver) : homePage;
    }

    public LoginPage getLoginPage() {
        return (loginPage == null) ? loginPage = new LoginPage(driver) : loginPage;
    }
}
