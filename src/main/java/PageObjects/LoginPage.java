package PageObjects;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class LoginPage {

    WebDriver driver;
    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 5), this);
    }

    @FindBy(xpath = "//span[@class='primary-nav__profile-text']" )
    private WebElement loginButton;

    @FindBy(id = "username")
    private WebElement username;

    @FindBy(id = "password")
    private WebElement password;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement submitButton;

    @FindBy(id = "error-page_title")
    private WebElement errorText;

    public void loginWithCredentials(String user, String pass) {
        loginButton.click();
        username.sendKeys(user);
        submitButton.click();
        password.sendKeys(pass);
        submitButton.click();
    }

    public void verifyTitleErrorPage() {
        Assert.assertEquals(errorText.getText(), "Inlogpoging geblokkeerd");
    }
}
