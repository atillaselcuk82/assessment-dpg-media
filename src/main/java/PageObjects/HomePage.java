package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class HomePage {

    WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 15), this);
    }

    @FindBy(xpath = "//p[@class='message-component text no-children title']")
    private WebElement privacySettingsTitle;

    @FindBy(xpath = "//button[@title='Akkoord']")
    private WebElement akkoordButton;

    @FindBy(xpath = "//button[@title='Instellen']")
    private WebElement settingsButton;

    @FindBy(xpath = "//button[@title='Alles aanvaarden']")
    private WebElement acceptAllButton;

    @FindBy(xpath = "(//h2[@class='ankeiler__title'])[4]")
    private WebElement titleFourthTile;

    @FindBy(xpath = "//label[@for='search-trigger']")
    private WebElement searchIcon;

    @FindBy(xpath = "//input[@class='input fjs-search-input']")
    private WebElement searchField;

    @FindBy(xpath = "//button[@class='submit']")
    private WebElement searchButton;

    @FindBy(xpath = "//button[@title='Voorkeuren opslaan']")
    private WebElement savePrefsButton;


    public void homePageIsDisplayed() {
        driver.switchTo().frame("sp_message_iframe_736203");
        privacySettingsTitle.isDisplayed();
    }

    public void acceptPrivacySettings() {
        akkoordButton.click();
    }

    public void clickFourthTile() {
        titleFourthTile.click();
    }

    public void searchArticle(String article) {
        searchIcon.click();
        searchField.sendKeys(article);
        searchButton.click();
    }

    public void selectRandomArticle() {
        driver.findElement(By.xpath(randomArticleXpathBuilder())).click();
    }

    private Integer randomInt() {
        int min = 1; // Minimum value of range
        int max = 10; // Maximum value of range
        return ((int)Math.floor(Math.random() * (max - min + 1) + min));
    }

    private String randomArticleXpathBuilder() {
        return "//li[@class='results__list-item']["+randomInt()+"]";
    }
}