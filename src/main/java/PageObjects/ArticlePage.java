package PageObjects;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class ArticlePage {

    WebDriver driver;

    public ArticlePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 15), this);
    }

    @FindBy(xpath = "//h1[@class='article__title']")
    private WebElement articleTitle;

    @FindBy(xpath = "//span[@aria-hidden='true']")
    private WebElement crossIcon;

    private void closeOfferIfDisplayed() {
        try {
            crossIcon.click();
        } catch (org.openqa.selenium.NoSuchElementException e) {
            System.out.println("Element not found, proceeding with next step");
        }
    }

    public void verifyArticleIsDisplayed() {
        closeOfferIfDisplayed();
        Assert.assertTrue(articleTitle.isDisplayed());
    }
}
