Feature: Browse main page
  Scenario: Browse Columnist on main page
    Given I navigate to AD.nl
    And accept privacy settings
    When I select a Columnist on the main page
    Then the article of the columnist is displayed