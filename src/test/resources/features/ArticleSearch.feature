Feature: Article Search
  Scenario: Search kickbox articles
    Given I navigate to AD.nl
    And accept privacy settings
    When I search for kickbox articles
    And Select a random article
    Then this article is displayed