package cucumbersteps;

import PageObjects.ArticlePage;
import PageObjects.HomePage;
import Utilities.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ArticleSearchStep {
    TestContext testContext;
    HomePage homePage;
    ArticlePage articlePage;

    public ArticleSearchStep(TestContext context) {
        testContext = context;
        homePage = testContext.getPageObjectManager().getHomePage();
        articlePage = testContext.getPageObjectManager().getArticlePage();
    }

    @When("I search for kickbox articles")
    public void iSearchForKickboxArticles() {
        homePage.searchArticle("Kickbox");
    }

    @And("Select a random article")
    public void selectARandomArticle() {
        homePage.selectRandomArticle();
    }

    @Then("this article is displayed")
    public void thisArticleIsDisplayed() {
        articlePage.verifyArticleIsDisplayed();
    }
}
