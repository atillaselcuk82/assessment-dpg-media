package cucumbersteps;

import PageObjects.HomePage;
import PageObjects.LoginPage;
import Utilities.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginStep {

    TestContext testContext;
    LoginPage loginPage;
    HomePage homePage;

    public LoginStep(TestContext context) {
        testContext = context;
        loginPage = testContext.getPageObjectManager().getLoginPage();
        homePage = testContext.getPageObjectManager().getHomePage();
    }

    @Given("I navigate to AD.nl")
    public void iNavigateToADNl() {
        homePage.homePageIsDisplayed();
    }

    @And("accept privacy settings")
    public void acceptPrivacySettings() {
        homePage.acceptPrivacySettings();
    }

    @When("I fill in my credentials to login")
    public void iFillInMyCredentialsToLogin() {
        loginPage.loginWithCredentials("atilla_selcuk@hotmail.com", "Rabbim1dir");
    }

    @Then("I am prompted with login attempt blocked page")
    public void iAmPromptedWithLoginAttemptBlockedPage() {
        loginPage.verifyTitleErrorPage();
    }
}
