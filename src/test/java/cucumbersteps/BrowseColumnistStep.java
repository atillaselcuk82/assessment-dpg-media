package cucumbersteps;

import PageObjects.ArticlePage;
import PageObjects.HomePage;
import Utilities.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class BrowseColumnistStep {

    TestContext testContext;
    HomePage homePage;
    ArticlePage articlePage;

    public BrowseColumnistStep(TestContext context) {
        testContext = context;
        homePage = testContext.getPageObjectManager().getHomePage();
        articlePage = testContext.getPageObjectManager().getArticlePage();
    }

    @When("I select a Columnist on the main page")
    public void iSelectAColumnistOnTheMainPage() {
        homePage.clickFourthTile();
    }

    @Then("the article of the columnist is displayed")
    public void theArticleOfTheColumnistIsDisplayed() {
        articlePage.verifyArticleIsDisplayed();
    }
}
