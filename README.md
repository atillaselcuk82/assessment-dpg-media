# Assessment DPG Media
This project is an evaluation of my proficiency in using Selenium for automating flow tests. The aim of this assessment is to demonstrate my understanding of the framework. The assessment will also be the basis of my 2nd intake subject. Below the assessment description:
>Site to be tested: https://www.ad.nl/
> 
> <u>**Use case 1:**</u>
> - Create a login scenario
> - You will need to register first in order to have valid login credentials (this a manual step)
>
> <u>**Use case 2:**</u>
> - Search for an article
> 
> <u>**Use case 3:**</u>
> - Browse to the main search page and navigate through a tile (i.e. Columnisten)

Based on this information I have written 3 flow tests with Java as programming language, Selenium as framework, IntelliJ as IDE and Maven as management tool to manage all dependencies in the pom.xml file.

# Framework Setup
Below the steps I have taken to properly setup the Framework:
* Install IntelliJ Community Edition: https://www.jetbrains.com/idea/download/#section=windows
* Add the following dependencies to pom.xml file. You can search for them on: https://mvnrepository.com/
  * junit
  * selenium-java
  * cucumber-java
  * cucumber-junit
* Enable Cucumber support: https://www.jetbrains.com/help/idea/enabling-cucumber-support-in-project.html
* Create feature files: https://www.jetbrains.com/help/idea/creating-feature-files.html
* Create step definitions: https://www.jetbrains.com/help/idea/creating-step-definition.html
* Run Cucumber tests: https://www.jetbrains.com/help/idea/running-cucumber-tests.html

# Running Tests
* Clone the repository
* Open the project using any Java IDE (f.e. IntelliJ)
* Run all the tests by right clicking ``recources`` and select Run all Features in recources
```
Run 'All Features in: recources'
```
* If you want to run a specific test (f.e. ArticleSearch), double click on the feature file > Run 'Feature ArticleSearch'

![img.png](img.png)

# Test Results
* Test report automatically generated in the console after the test execution has finished
![img_1.png](img_1.png)

* For more information about reports cucumber you can go to https://reports.cucumber.io/docs/cucumber-jvm